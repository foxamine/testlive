﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    public bool isLive = false;
    public bool nextStep = false;
    public Material whiteMaterial;
    public Material blackMaterial;
    [HideInInspector]
    public Transform cachedTransform;
    [HideInInspector]
    public MeshRenderer meshRenderer;


    public void ApplyLive()
    {
        isLive = nextStep;
        if (isLive)
        {
            meshRenderer.sharedMaterial = blackMaterial;
        }
        else
        {
            meshRenderer.sharedMaterial = whiteMaterial;
        }
    }

    public void SetState(bool live)
    {
        nextStep = live;
        if (!CellController.cellController.isRun) ApplyLive();
    }

    public void SetState()
    {
        SetState(!nextStep);
    }

    private void OnEnable()
    {
        if (meshRenderer != null) meshRenderer.sharedMaterial = whiteMaterial;
        else
        {
            meshRenderer = GetComponent<MeshRenderer>();
            meshRenderer.sharedMaterial = whiteMaterial;
        }
        isLive = false;
        nextStep = false;
    }
}
