﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Text playStopLbl;
    public Slider speedSlider;
    public Slider sizeSlider;
    public GameObject randomBtn;
    public Toggle toggle3d;
    public GameObject generateBtn;

    private CellController cellController;
    private int currentSize = 1;

    public void OnPlayBTN()
    {
        CellController.cellController.SetGameState(!CellController.cellController.isRun);

        if (CellController.cellController.isRun)
        {
            
            playStopLbl.text = "Стоп";
            toggle3d.gameObject.SetActive(false);
            sizeSlider.gameObject.SetActive(false);
            randomBtn.SetActive(false);
            generateBtn.SetActive(false);
        }
        else
        {
            playStopLbl.text = "Старт";
            toggle3d.gameObject.SetActive(true);
            sizeSlider.gameObject.SetActive(true);
            randomBtn.SetActive(true);
            generateBtn.SetActive(true);
        }
    }

    public void OnRandowmBTN()
    {
        CellController.cellController.RandomLive();
    }

    public void OnChangeSpeed()
    {
        CellController.cellController.SetGameSpeed(speedSlider.value);
    }

    public void OnChancheSize()
    {
        currentSize = (int)sizeSlider.value;
    }

    public void Set3DState()
    {
        CellController.cellController.SetDimension(toggle3d.isOn);
    }

    public void Generate()
    {
        CellController.cellController.GenerateCells(currentSize, currentSize, currentSize);
    }
}
