﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellController : MonoBehaviour
{
    public static CellController cellController;

    public bool generate3d = false;
    private bool current3dState = false;
    public bool isRun = false;

    private float gameSpeed = .5f;
    private Cell[,,] cells;
    private float offset = 1;
    private Coroutine gameLoopCoroutine;

    private int xUpper;
    private int yUpper;
    private int zUpper;

    private void Clear()
    {
        foreach (Cell T in cells)
        {
            CellPool.Despawn(T);
        }
    }

    public void GenerateCells(int x, int y, int z)
    {
        if (cells != null) Clear();
        
        SetDimension(generate3d);
        current3dState = generate3d;

        if (!current3dState) z = 1;
       
        cells = new Cell[x, y, z];
        Vector3 cubeHalfSize = new Vector3(x, y, z) * offset * .5f;
        xUpper = cells.GetUpperBound(0);
        yUpper = cells.GetUpperBound(1);
        zUpper = cells.GetUpperBound(2);

        for (int i = 0; i < x ; i++)
        {
            for (int n = 0; n < y; n++)
            {
                for (int o = 0; o < z; o++)
                {
                    cells[i, n, o] = CellPool.Spawn();
                    cells[i, n, o].cachedTransform.position = new Vector3(i, n, o) * offset - cubeHalfSize;
                }
            }
        }
    }

    public void RandomLive()
    {
        if (cells == null) return;

        foreach (Cell T in cells)
        {
            if (UnityEngine.Random.Range(0, 1f) > .5f) T.SetState(true);
            else T.SetState(false);
        }
    }
    
    public void SetGameSpeed(float speed)
    {
        gameSpeed = speed * .1f;
    }

    public void SetGameState(bool state)
    {
        if (cells == null) return;

        isRun = state;
        if (isRun) gameLoopCoroutine = StartCoroutine(GameLoop());
        else StopCoroutine(gameLoopCoroutine);
    }

    public void SetDimension(bool is3D)
    {
        generate3d = is3D;
        if (is3D) offset = 1.5f;
        else offset = 1;
    }


    private bool GetPointCondition(int x, int y, int z)
    {
        if (x < 0) x = xUpper;
        if (y < 0) y = yUpper;
        if (z < 0) z = zUpper;

        if (x > xUpper) x = 0;
        if (y > yUpper) y = 0;
        if (z > zUpper) z = 0;

        if (cells[x, y, z].isLive) return true;
        else return false;
    }

    int GetNeighborCount(int x, int y, int z)
    {
        int result = 0;
        for (int i = -1; i <= 1; i++)
        {
            for (int n = -1; n <= 1; n++)
            {
                if (generate3d)
                {
                    for (int o = -1; o <= 1; o++)
                    {
                        if (0 == i && 0 == n && 0 == o) continue;
                        if (GetPointCondition(x + i, y + n, z + o)) result++;
                    }
                }
                else
                {
                    if (0 == i && 0 == n) continue;
                    if (GetPointCondition(x + i, y + n, 0)) result++;
                }
            }
        }

        return result;
    }

    private void Start()
    {
        cellController = this;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1)) GenerateCells(1, 1, 1);
        if (Input.GetKeyDown(KeyCode.Alpha2)) GenerateCells(2, 2, 2);
        if (Input.GetKeyDown(KeyCode.Alpha3)) GenerateCells(3, 3, 3);
        if (Input.GetKeyDown(KeyCode.Alpha4)) GenerateCells(4, 4, 4);
        if (Input.GetKeyDown(KeyCode.Alpha5)) GenerateCells(5, 5, 5);
        if (Input.GetKeyDown(KeyCode.Alpha6)) GenerateCells(6, 6, 6);
        if (Input.GetKeyDown(KeyCode.Alpha7)) GenerateCells(7, 7, 7);
        if (Input.GetKeyDown(KeyCode.Alpha8)) GenerateCells(8, 8, 8);
        if (Input.GetKeyDown(KeyCode.Alpha9)) GenerateCells(9, 9, 9);
        if (Input.GetKeyDown(KeyCode.Alpha0)) GenerateCells(10, 10, 10);

        if (Input.GetKeyDown(KeyCode.Escape)) Clear();
        if (Input.GetKeyDown(KeyCode.R)) RandomLive();
        if (Input.GetKeyDown(KeyCode.Space)) SetGameState(!isRun);
        
        if (Input.GetKeyDown(KeyCode.Keypad1)) SetGameSpeed(1);
        if (Input.GetKeyDown(KeyCode.Keypad2)) SetGameSpeed(2);
        if (Input.GetKeyDown(KeyCode.Keypad3)) SetGameSpeed(3);
        if (Input.GetKeyDown(KeyCode.Keypad4)) SetGameSpeed(4);
        if (Input.GetKeyDown(KeyCode.Keypad5)) SetGameSpeed(5);
        if (Input.GetKeyDown(KeyCode.Keypad6)) SetGameSpeed(6);
        if (Input.GetKeyDown(KeyCode.Keypad7)) SetGameSpeed(7);
        if (Input.GetKeyDown(KeyCode.Keypad8)) SetGameSpeed(8);
        if (Input.GetKeyDown(KeyCode.Keypad9)) SetGameSpeed(9);
        if (Input.GetKeyDown(KeyCode.Keypad0)) SetGameSpeed(10);

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo = new RaycastHit();
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo))
            {
                hitInfo.collider.GetComponent<Cell>().SetState();
            }
        }
    }
    
    IEnumerator GameLoop()
    {
        while (true)
        {
            if (cells == null) break;

            for (int i = 0; i <= xUpper; i++)
            {
                for (int n = 0; n <= yUpper; n++)
                {
                    for (int o = 0; o <= zUpper; o++)
                    {
                        int neighbors = GetNeighborCount(i, n, o);
                        bool cellIsLive = cells[i, n, o].isLive;

                        if (!cellIsLive && neighbors == 3)//если клетка мертвая и есть 3 соседа
                        {
                            cells[i, n, o].SetState(true);
                            continue;
                        }
                        if (cellIsLive && neighbors >= 2 && neighbors <= 3) cells[i, n, o].SetState(true);
                        else cells[i, n, o].SetState(false);
                    }
                }
            }

            foreach (Cell T in cells)
            {
                T.ApplyLive();
            }
            
            yield return new WaitForSeconds(gameSpeed);
        }

    }
}
