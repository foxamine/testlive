﻿using System.Collections.Generic;
using UnityEngine;

public class CellPool : MonoBehaviour
{
    public static CellPool cellPool;
    public Cell cellPrefab;
    private static Cell cellPrefStatic;
    private static List<Cell> pool = new List<Cell>();


    public static Cell Spawn()
    {
        Cell result;

        if (pool.Count > 0)
        {
            result = pool[0];
            pool.RemoveAt(0);
            result.gameObject.SetActive(true);
        }
        else
        {
            result = Instantiate(cellPrefStatic);
            result.cachedTransform = result.transform;
        }
        return result;
    }
    
    public static void Despawn(Cell go)
    {
        pool.Add(go);
        go.gameObject.SetActive(false);
    }

    private void Start()
    {
        cellPool = this;
        cellPrefStatic = cellPrefab;
    }
}
