﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitCamera : MonoBehaviour
{
    public float xRot = 0f;
    public float yRot = 0f;
    private float cameraDistance;
    private Transform cameraTransform;

    private void Start()
    {
        cameraDistance = transform.position.z;
        cameraTransform = transform;
    }

    void Update()
    {
        if (Input.GetMouseButton(1) || Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            xRot += Input.GetAxis("Mouse Y") * 500 * Time.deltaTime;
            yRot += Input.GetAxis("Mouse X") * 500 * Time.deltaTime;
            cameraDistance -= Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * 500;

            if (xRot > 90f)
            {
                xRot = 90f;
            }
            else if (xRot < -90f)
            {
                xRot = -90f;
            }

            cameraTransform.position = Vector3.zero + Quaternion.Euler(xRot, yRot, 0f) * (Vector3.forward * cameraDistance);
            cameraTransform.LookAt(Vector3.zero, Vector3.up);
        }
    }
}
